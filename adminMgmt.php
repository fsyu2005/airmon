<?php 
ob_start();
session_start();
require_once './utility/ArrayList.php';
if(!isset($_SESSION['acct'])){
	// 尚未登入
	header("Location:index.php");
}
?>

<!DOCTYPE html>
<html>
<head>      
	<?php include("include/header.php") ?>
</head>
<style>
	.panel-heading{
		font-size:24px;
		font-weight:600;
	}
</style>

<body>
	<!-- 導覽列：Start -->
	<?php 
	if(isset($_SESSION['acct']) ){
		// echo "test";
		include("include/loginNavBar.php"); 
	}else{
		// echo "noooo";
		include("include/logoutNavBar.php"); 
	}
	?>
	<!-- END: 導覽列 -->


	<!-- 內容區：Start 程式碼寫在這 --> 
	<div class="container container_min">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
			<!-- 管理者清單 -->
			<div class="panel panel-info">
				<div class="panel-heading">管理者清單
					<button type="button" class="btn btn-danger ml-3 pull-right" data-toggle="modal" data-target="#removeadm">移除</button>
					<button type="button" class="btn btn-warning ml-3 pull-right" data-toggle="modal" data-target="#modadm">修改</button>
					<button type="button" class="btn btn-success ml-3 pull-right " data-toggle="modal" data-target="#addadm">新增</button>
				</div>
				<div class="panel-body">
					<div class="row">
						<ul id="list">
						</ul>
					</div>
				</div>
			</div>
			</div>
		</div>
			 
		<!-- 新增管理者 -->
		<div class="modal fade" id="addadm" role="dialog">
			<div class="modal-dialog modal-md">
				<div class="modal-content">
					<div class="modal-header green">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">新增管理者</h4>
					</div>

					<div class="modal-body">
						<form id="addadmform" name="addadmform" method="post" onclick="return false">
							<div class="row">
								<!-- 管理者名稱 -->
								<div class="col-md-6">
									<div class="form-group">
										<label for="addname">管理者名稱:</label>
										<input type="text" class="form-control" name="addname" id="addname">
									</div>
								</div>
								<!-- 管理者密碼 -->
								<div class="col-md-6">
									<div class="form-group">
										<label for="addpwd">管理者密碼:</label>
										<input type="password" class="form-control" name="addpwd" id="addpwd">
									</div>
								</div>
							</div>
						</form>
					</div>

					<div class="modal-footer">
						<button id="submit_add_adm" class="btn btn-success pull-left"><span class="glyphicon glyphicon-plus"></span>新增</button>
						<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
					</div>

				</div>
			</div>
		</div>
		
		<!-- 密碼修改 -->
		<div class="modal fade" id="modadm" role="dialog">
			<div class="modal-dialog modal-md">
				<div class="modal-content">
					<div class="modal-header yellow">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">密碼修改</h4>
					</div>

					<div class="modal-body">
						<form id="modadmform" name="modadmform" method="post" onclick="return false">
							<div class="row">
								<!-- 選擇管理者 -->
								<div class="col-md-6">
									<div class="form-group">
										<label for="modadmin">選擇管理者:</label>
										<select class="form-control" name="modadmin" id="modadmin">
											
										</select>
									</div>
								</div>
							</div>	
							<div class="row">	
								<!-- 輸入舊密碼 -->
								<div class="col-md-6">
									<div class="form-group">
										<label for="oldpwd">舊密碼:</label>
										<input type="password" class="form-control" name="oldpwd" id="oldpwd">
									</div>
								</div>
								<!-- 管理者密碼 -->
								<div class="col-md-6">
									<div class="form-group">
										<label for="newpwd">新密碼:</label>
										<input type="password" class="form-control" name="newpwd" id="newpwd">
									</div>
								</div>
							</div>
						</form>
					</div>

					<div class="modal-footer">
						<button id="submit_mod_adm" class="btn btn-warning pull-left"><span class="glyphicon glyphicon-pencil"></span>修改</button>
						<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
					</div>

				</div>
			</div>
		</div>
		
		<!-- 移除管理者 -->
		<div class="modal fade" id="removeadm" role="dialog">
			<div class="modal-dialog modal-md">
				<div class="modal-content">
					<div class="modal-header red">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">移除管理者</h4>
					</div>

					<div class="modal-body">
					<form id="removeadmform" name="removeadmform" method="post" onclick="return false">
						<div class="row">
							<!-- 選擇管理者 -->
							<div class="col-md-6">
								<div class="form-group">
									<label for="removeadmin">選擇管理者:</label>
									<select class="form-control" name="removeadmin" id="removeadmin">
										
									</select>
								</div>
							</div>
							<!-- 管理者密碼 -->
							<div class="col-md-6">
								<div class="form-group">
									<label for="removepwd">輸入密碼:</label>
									<input type="password" class="form-control" name="removepwd" id="removepwd">
								</div>
							</div>
						</div>
					</form>						
					</div>

					<div class="modal-footer">
						<button id="submit_remove_adm" class="btn btn-danger pull-left"><span class="glyphicon glyphicon-remove"></span>移除</button>
						<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
					</div>

				</div>
			</div>
		</div>


	</div>
	<!-- 內容區：END -->

	

    <!-- Footer列：Start -->
	<?php include("include/footer.php") ?>
	<!-- Footer列：End -->
</body>
</html>

<script>
	
$(document).ready(function(){
	// 載入管理者清單
	$.ajax({
		url: "Controller.php?command=GetAdmin",
		type: "POST",
		dataType: "json",
			success: function(list) {
				for (i = 0; i < list.length; i++) {
					// $("#modadmin").append("<option value='"+list[i]+"'>"+list[i]+"</option>");
					$("#removeadmin").append("<option value='"+list[i]+"'>"+list[i]+"</option>");
					$("#list").append($("<li>").text(list[i]));
				}
				$("#removeadmin").find("option").first().remove();
				},
			error: function() {
				alert("ERROR!!!");
				}
	});
	// 新增管理者
	$("#submit_add_adm").on('click', function(){
		if($("#addname").val().replace(/\s+/g,"")==""){
            alert("未填寫管理者名稱");
            eval("document.addadmform['addname'].focus()");       
        }else if($("#addpwd").val().replace(/\s+/g,"")==""){
            alert("未填寫密碼");
            eval("document.addadmform['addpwd'].focus()");    
        }else{
			$.ajax({
			url: 'Controller.php?command=Addadmin',
			type : "POST",
			dataType : 'json',
			data : $("#addadmform").serialize(),
				success : function(result) {
					alert(result);
				},
				error: function(result) {
					console.log(result);
				}
			});
			
			location.replace('adminMgmt.php');

        }
		
	});

	// 修改密碼
	$("#submit_mod_adm").on('click', function(){
		if($("#oldpwd").val().replace(/\s+/g,"")==""){
            alert("請輸入舊密碼");
            eval("document.modadmform['oldpwd'].focus()");       
        }else if($("#newpwd").val().replace(/\s+/g,"")==""){
            alert("請輸入新密碼");
            eval("document.modadmform['newpwd'].focus()");    
        }else{
			$.ajax({
			url: 'Controller.php?command=ModifyAdmin',
			type : "POST",
			dataType : 'json',
			data : $("#modadmform").serialize(),
				success : function(result) {
					alert(result);
				},
				error: function(result) {
					console.log(result);
				}
			});

			location.replace('adminMgmt.php');
        }
		
	});

	// 移除密碼
	$("#submit_remove_adm").on('click', function(){
		if($("#removepwd").val().replace(/\s+/g,"")==""){
            alert("請輸入密碼");
            eval("document.removeadmform['removepwd'].focus()");       
        }else{
			$.ajax({
			url: 'Controller.php?command=RemoveAdmin',
			type : "POST",
			dataType : 'json',
			data : $("#removeadmform").serialize(),
				success : function(result) {
					alert(result);
					
					
				},
				error: function(result) {
					console.log(result);
				}
			});
			
			location.replace('Controller.php?command=Logout');
        }
		
	});
	

});
	

	
</script>