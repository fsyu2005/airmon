<?php 
ob_start();
session_start();
require_once './utility/ArrayList.php';

?>

<!DOCTYPE html>
<html>

<head>

	<?php include("include/header.php") ?>


</head>


<body>
	<!-- 導覽列：Start -->
	<?php 
	if(isset($_SESSION['acct']) ){
		// echo "test";
		include("include/loginNavBar.php"); 
	}else{
		// echo "noooo";
		include("include/logoutNavBar.php"); 
	}
	?>
	<!-- END: 導覽列 -->


	<!-- 內容區：Start 程式碼寫在這 -->
	<div class="container container_min">

		<h1>關於我們&nbsp;<small></small> </h1>
		<hr>
		<div class="row">
			<div class="col-md-4" style="position:relative;">
				<div class="row dataBox-mainBox" style="cursor: pointer;">
					<div class="col-md-5" style="padding:10px;">
						<div class="dataBox-imgBox" style="background-image: url(image/aboutus/游.jpg); background-repeat: no-repeat; background-size: contain;"></div>
						<div class="dataBox-classBox">
							<div class="dataBox-classBox-text">游峰碩</div>
							<div class="dataBox-classBox-fold"></div>
						</div>

						<div class="dataBox-cardBox">
							<div class="dataBox-cardBox-text">指導老師 </div>
							<div class="dataBox-cardBox-fold"></div>
						</div>

					</div>

					<div class="col-md-7" style="padding:10px;">
						<div class="dataBox-nameBox">
							<h4 style="margin:5px 0px; font-weight:bold;">游の工作項目</h4>
						</div>

						<div class="dataBox-addrBox">
							<div class="text-ellipsis text-ellipsis-2 data-addr-1" >
								<p>監督專題的工作進度</p>
								<p>解決軟硬體方面各項問題</p>
								<p>協助系統及Arduino相關技術</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4" style="position:relative;">
				<div class="row dataBox-mainBox" style="cursor: pointer;">
					<div class="col-md-5" style="padding:10px;">
						<div class="dataBox-imgBox" style="background-image: url(image/aboutus/杜.jpg); background-repeat: no-repeat; background-size: contain;"></div>
						<div class="dataBox-classBox">
							<div class="dataBox-classBox-text">杜名揚</div>
							<div class="dataBox-classBox-fold"></div>
						</div>

						<div class="dataBox-cardBox">
							<div class="dataBox-cardBox-text">組長</div>
							<div class="dataBox-cardBox-fold"></div>
						</div>

					</div>

					<div class="col-md-7" style="padding:10px;">
						<div class="dataBox-nameBox">
							<h4 style="margin:5px 0px; font-weight:bold;">羊の工作項目</h4>
						</div>

						<div class="dataBox-addrBox">
							<div class="text-ellipsis text-ellipsis-2 data-addr-1" >
								<p>Arduino硬體製作與設計</p>
								<p>監控系統製作與設計</p>
								
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4" style="position:relative;">
				<div class="row dataBox-mainBox" style="cursor: pointer;">
					<div class="col-md-5" style="padding:10px;">
						<div class="dataBox-imgBox" style="background-image: url(image/aboutus/毛.jpg); background-repeat: no-repeat; background-size: contain;"></div>
						<div class="dataBox-classBox">
							<div class="dataBox-classBox-text">毛嘉萱</div>
							<div class="dataBox-classBox-fold"></div>
						</div>

						<div class="dataBox-cardBox">
							<div class="dataBox-cardBox-text">組員</div>
							<div class="dataBox-cardBox-fold"></div>
						</div>

					</div>

					<div class="col-md-7" style="padding:10px;">
						<div class="dataBox-nameBox">
							<h4 style="margin:5px 0px; font-weight:bold;">毛の工作項目</h4>
						</div>

						<div class="dataBox-addrBox">
							<div class="text-ellipsis text-ellipsis-2 data-addr-1" >
								<p>WORD文件撰寫</p>
								<p>PPT製作</p>
								<p>海報製作(中文版)</p>
								<p>美術編輯</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-4" style="position:relative;">
				<div class="row dataBox-mainBox" style="cursor: pointer;">
					<div class="col-md-5" style="padding:10px;">
						<div class="dataBox-imgBox" style="background-image: url(image/aboutus/曾.jpg); background-repeat: no-repeat; background-size: contain;"></div>
						<div class="dataBox-classBox">
							<div class="dataBox-classBox-text">曾華鏞</div>
							<div class="dataBox-classBox-fold"></div>
						</div>

						<div class="dataBox-cardBox">
							<div class="dataBox-cardBox-text">組員</div>
							<div class="dataBox-cardBox-fold"></div>
						</div>

					</div>

					<div class="col-md-7" style="padding:10px;">
						<div class="dataBox-nameBox">
							<h4 style="margin:5px 0px; font-weight:bold;">曾の工作項目</h4>
						</div>

						<div class="dataBox-addrBox">
							<div class="text-ellipsis text-ellipsis-2 data-addr-1" >
								<p>DEMO影片製作</p>
								<p>攝影、照片美化</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4" style="position:relative;">
				<div class="row dataBox-mainBox" style="cursor: pointer;">
					<div class="col-md-5" style="padding:10px;">
						<div class="dataBox-imgBox" style="background-image: url(image/aboutus/許.jpg); background-repeat: no-repeat; background-size: contain;"></div>
						<div class="dataBox-classBox">
							<div class="dataBox-classBox-text">許建華</div>
							<div class="dataBox-classBox-fold"></div>
						</div>

						<div class="dataBox-cardBox">
							<div class="dataBox-cardBox-text">組員 </div>
							<div class="dataBox-cardBox-fold"></div>
						</div>

					</div>

					<div class="col-md-7" style="padding:10px;">
						<div class="dataBox-nameBox">
							<h4 style="margin:5px 0px; font-weight:bold;">華の工作項目</h4>
						</div>

						<div class="dataBox-addrBox">
							<div class="text-ellipsis text-ellipsis-2 data-addr-1" >
								<p>後臺資料庫的規劃設計與維護</p>
								<p>海報製作(英文版)</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4" style="position:relative;">
				<div class="row dataBox-mainBox" style="cursor: pointer;">
					<div class="col-md-5" style="padding:10px;">
						<div class="dataBox-imgBox" style="background-image: url(image/aboutus/素.jpg); background-repeat: no-repeat; background-size: contain;"></div>
						<div class="dataBox-classBox">
							<div class="dataBox-classBox-text">葉素綺</div>
							<div class="dataBox-classBox-fold"></div>
						</div>

						<div class="dataBox-cardBox">
							<div class="dataBox-cardBox-text">組員 </div>
							<div class="dataBox-cardBox-fold"></div>
						</div>

					</div>

					<div class="col-md-7" style="padding:10px;">
						<div class="dataBox-nameBox">
							<h4 style="margin:5px 0px; font-weight:bold;">素の工作項目</h4>
						</div>

						<div class="dataBox-addrBox">
							<div class="text-ellipsis text-ellipsis-2 data-addr-1" >
									<p>WORD文件撰寫</p>
									<p>海報製作</p>
									<p>美術編輯</p>
							</div>
						</div>
					</div>
				</div>
			</div>








		</div>




	</div>
	<!-- 內容區：END -->
	


	<!-- Footer列：Start -->
	<?php include("include/footer.php") ?>
	<!-- Footer列：End -->
</body>

</html>