<?php 
ob_start();
session_start();
require_once './utility/ArrayList.php';
if(!isset($_SESSION['acct'])){
	// 尚未登入
	header("Location:index.php");
}
?>

<!DOCTYPE html>
<html>
<head>      
	<?php include("include/header.php") ?>
</head>


<body>
	<!-- 導覽列：Start -->
	<?php 
	if(isset($_SESSION['acct']) ){
		// echo "test";
		include("include/loginNavBar.php"); 
	}else{
		// echo "noooo";
		include("include/logoutNavBar.php"); 
	}
	?>
	<!-- END: 導覽列 -->


	<!-- 內容區：Start 程式碼寫在這 --> 
	
	<!-- 內容區：END -->

	

    <!-- Footer列：Start -->
	<?php include("include/footer.php") ?>
	<!-- Footer列：End -->
</body>
</html>