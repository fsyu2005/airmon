<?php 
ob_start();
session_start();
require_once './utility/ArrayList.php';
// if(!isset($_SESSION['acct'])){
// 	// 尚未登入
// 	header("Location:index.php");
// }
?>
<!DOCTYPE html>
<html>

<head>      
<?php include("include/header.php") ?>


</head>
<style>
	.panel-heading{
		font-size:24px;
		font-weight:600;
		}
	.explain > tbody > tr > td ,.explain > thead > tr > th {
		padding: 5px 20px;
		text-align: center;
	}
</style>


<body onload="get();">
	<!-- 導覽列：Start -->
	<?php 
	if(isset($_SESSION['acct']) ){
		// echo "test";
		include("include/loginNavBar.php"); 
	}else{
		// echo "noooo";
		include("include/logoutNavBar.php"); 
	}
	?>
	<!-- END: 導覽列 -->

	<div class="container container_min">
		<h1>依教室查詢&nbsp;<small>當前數據</small> </h1>	
		<!-- 說明 -->
		<div class="alert alert-info">
			<div class="row">
				<div class="col-md-12">
					<h3 style="margin: 0 0 15px 0;"><strong>查詢條件</strong></h3>
					<strong><h4>說明：</h4></strong>
					<p>　※選擇欲查詢之教室後，即可得知此教室即時的溫度、濕度、二氧化碳濃度數據</p>
				</div>
				<div class="col-md-12">
					<table class="explain">
						<thead>
							<tr>
								<th>指標</th>
								<th>溫度</th>
								<th>濕度</th>
								<th>二氧化碳</th>
							</tr>
						</thead>
						<tbody>
						<tr>
							<td><span class="label label-info">過低</span></td>
							<td>18°C ↓</td>
							<td>40% ↓</td>
							<td>-</td>
						</tr>
						<tr>
							<td><span class="label label-success">正常</span></td>
							<td>18°C ~ 24°C </td>
							<td>40% ~ 60%</td>
							<td>0ppm ~ 400ppm</td>
						</tr>
						<tr>
							<td><span class="label label-warning">尚可</span></td>
							<td>24°C ~ 28°C</td>
							<td>60% ~ 70%</td>
							<td>400ppm ~ 600ppm</td>
						</tr>
						<tr>
							<td><span class="label label-danger">不良</span></td>
							<td>28°C ↑</td>
							<td>70% ~ 100%</td>
							<td>600ppm ↑</td>
						</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-3">
					<form id="roomform" name="roomform" method="post">
						<div class="form-group">
							<label for="sel_room">選擇教室:</label>
							<select class="form-control" id="sel_room" name="sel_room">
								<!-- <option selected="true" disabled="true">請選擇</option> -->
							</select>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- 依教室查詢當前數據 -->
		<div class="panel panel-success">
			<div class="panel-heading" id="roomname"></div>
			<div class="panel-body">
				<!-- 選擇教室 -->
				
				<!-- 教室數據表 -->
				<div class="row">
					<div class="col-md-4 col-sm-12 text-center" id="Temp">
						<div id="currTemp"></div>     
						
					</div>

					<div class="col-md-4 col-sm-12 text-center">
						<div id="currHumi"></div>
						
					</div>  

					<div class="col-md-4 col-sm-12 text-center">
						<div id="currCo2"></div>  
						
					</div>
				</div>

			</div>
		</div>
	</div>	
	<!-- Footer列：Start -->
	<?php include("include/footer.php") ?>
	<!-- Footer列：End -->
<script>
	$(document).ready(function(){
		
		// 載入教室清單
		$.ajax({
			url: "Controller.php?command=GetRoom",
			type: "POST",
			dataType: "json",
				success: function(list) {
					var classroom ;
					for (i = 0; i < list.length; i++) {
						if( i==0 ){
							classroom = list[i]["name"];
						}

						$("#sel_room").append("<option value='"+list[i]["code"]+"'>"+list[i]["name"]+"</option>");
					}
					$("#roomname").html(classroom +"-"+"當前數據");
				},
				error: function() {
					alert("ERROR!!!");
				}
		});
		
	

		setInterval('get();',10*1000);//10s执行一次
		// 取得教室當前數據
		$("#sel_room").change( function(){
			$.ajax({
				url: 'Controller.php?command=GetBoard',
				type : "POST",
				dataType : 'json',
				data : $("#roomform").serialize(),
				success : function(result) {

					if ($("svg").length > 0 ){
						console.log("清除圖表")
						$("svg").remove();
					}else{
						console.log("不清")
					}
					
					var temp =result["temp"];
					var Humi =result["humi"];
					var co2 = result["cotwo"];
					var TempOpt;
					var HumiOpt;
					var Co2Opt;
					
					if(temp>28){
						TempOpt={
							foregroundBorderWidth: 8,
							backgroundBorderWidth: 10,
							foregroundColor:'#E74C3C',//紅
							backgroundColor:'#D6DBDF',
							fillColor:'#eee',	//填充圓的顏色
							replacePercentageByText:result["temp"]+"°C",
							textColor:'#337ab7',//文字顏色
							textBelow:'true',
							text:"溫度",
							textStyle:'font-size: 14px;font-weight: 600;',
							percent: result["temp"],
							halfCircle: false,
						};
						
					}else if(temp>24){

						TempOpt={
							foregroundBorderWidth: 8,//前景圈的顏色
							backgroundBorderWidth: 10,//背景圓的顏色
							foregroundColor:'#F39C12',//橘
							backgroundColor:'#D6DBDF',
							fillColor:'#eee',	//填充圓的顏色
							replacePercentageByText:result["temp"]+"°C",
							textColor:'#337ab7',//文字顏色
							textBelow:'true',
							text:"溫度",
							textStyle:'font-size: 14px;font-weight: 600;',
							percent: result["temp"],
							halfCircle: false,
						};

					}else if(temp>18){

						TempOpt={
							foregroundBorderWidth: 8,//前景圈的顏色
							backgroundBorderWidth: 10,//背景圓的顏色
							foregroundColor:'#5cb85c',//綠
							backgroundColor:'#D6DBDF',
							fillColor:'#eee',	//填充圓的顏色
							replacePercentageByText:result["temp"]+"°C",
							textColor:'#337ab7',//文字顏色
							textBelow:'true',
							text:"溫度",
							textStyle:'font-size: 14px;font-weight: 600;',
							percent: result["temp"],
							halfCircle: false,
						};	

					}else{

						TempOpt={
							foregroundBorderWidth: 8,//前景圈的顏色
							backgroundBorderWidth: 10,//背景圓的顏色
							foregroundColor:'#3498DB',//藍
							backgroundColor:'#D6DBDF',
							fillColor:'#eee',	//填充圓的顏色
							replacePercentageByText:result["temp"]+"°C",
							textColor:'#337ab7',//文字顏色
							textBelow:'true',
							text:"溫度",
							textStyle:'font-size: 14px;font-weight: 600;',
							percent: result["temp"],
							halfCircle: false,
						};
					};

					if(Humi>70){
						HumiOpt={
							foregroundBorderWidth: 8,
							backgroundBorderWidth: 10,
							foregroundColor:'#E74C3C',//紅
							backgroundColor:'#D6DBDF',
							fillColor:'#eee',	//填充圓的顏色
							textColor:'#337ab7',//文字顏色
							textBelow:'true',
							text:"濕度",
							textStyle:'font-size: 14px;font-weight: 600;',
							percent: result["humi"],
							halfCircle: false,
						};
						
					}else if(Humi>60){
						HumiOpt={
							foregroundBorderWidth: 8,//前景圈的顏色
							backgroundBorderWidth: 10,//背景圓的顏色
							foregroundColor:'#F39C12',//橘
							backgroundColor:'#D6DBDF',
							fillColor:'#eee',	//填充圓的顏色
							textColor:'#337ab7',//文字顏色
							textBelow:'true',
							text:"濕度",
							textStyle:'font-size: 14px;font-weight: 600;',
							percent: result["humi"],
							halfCircle: false,
						};
					}else if(Humi>40){
						HumiOpt={
							foregroundBorderWidth: 8,//前景圈的顏色
							backgroundBorderWidth: 10,//背景圓的顏色
							foregroundColor:'#5cb85c',//綠
							backgroundColor:'#D6DBDF',
							fillColor:'#eee',	//填充圓的顏色
							textColor:'#337ab7',//文字顏色
							textBelow:'true',
							text:"濕度",
							textStyle:'font-size: 14px;font-weight: 600;',
							percent: result["humi"],
							halfCircle: false,
						};
					}else{
						HumiOpt={
							foregroundBorderWidth: 8,//前景圈的顏色
							backgroundBorderWidth: 10,//背景圓的顏色
							foregroundColor:'#3498DB',//藍
							backgroundColor:'#D6DBDF',
							fillColor:'#eee',	//填充圓的顏色
							textColor:'#337ab7',//文字顏色
							textBelow:'true',
							text:"濕度",
							textStyle:'font-size: 14px;font-weight: 600;',
							percent: result["humi"],
							halfCircle: false,
						};							
					};

					if(co2>600){
						Co2Opt={
							foregroundBorderWidth: 8,//前景圈的顏色
							backgroundBorderWidth: 10,//背景圓的顏色
							foregroundColor:'#E74C3C',//紅
							backgroundColor:'#D6DBDF',
							fillColor:'#eee',	//填充圓的顏色
							noPercentageSign:'true',//隱藏百分比
							textColor:'#337ab7',//文字顏色
							textBelow:'true',
							text:"二氧化碳",
							textStyle:'font-size: 14px;font-weight: 600;',
							percent: result["cotwo"],
							halfCircle: false,
							animationStep:80,
						};
						
					}else if(co2>400){
						Co2Opt={
							foregroundBorderWidth: 8,//前景圈的顏色
							backgroundBorderWidth: 10,//背景圓的顏色
							foregroundColor:'#F39C12',//橘
							backgroundColor:'#D6DBDF',
							fillColor:'#eee',	//填充圓的顏色
							noPercentageSign:'true',//隱藏百分比
							textColor:'#337ab7',//文字顏色
							textBelow:'true',
							text:"二氧化碳",
							textStyle:'font-size: 14px;font-weight: 600;',
							percent: result["cotwo"],
							halfCircle: false,
							animationStep:80,
						};
					}else{
						Co2Opt={
							foregroundBorderWidth: 8,//前景圈的顏色
							backgroundBorderWidth: 10,//背景圓的顏色
							foregroundColor:'#5cb85c',//綠
							backgroundColor:'#D6DBDF',
							fillColor:'#eee',	//填充圓的顏色
							noPercentageSign:'true',//隱藏百分比
							textColor:'#337ab7',//文字顏色
							textBelow:'true',
							text:"二氧化碳",
							textStyle:'font-size: 14px;font-weight: 600;',
							percent: result["cotwo"],
							halfCircle: false,
							animationStep:80,
						};							
					};

					$('#currTemp').circliful(TempOpt);
					
					$('#currHumi').circliful(HumiOpt);
					$('#currCo2').circliful(Co2Opt);  


				},
				error: function(result) {
					console.log(result);
				}
			});

			var options=$("#sel_room option:selected");
			
			$("#roomname").html(options.text()+"-"+"當前數據");


		});

		
	});
	
	function get(){
		$.ajax({
			url: 'Controller.php?command=GetBoard',
			type : "POST",
			dataType : 'json',
			data : $("#roomform").serialize(),
			success : function(result) {

				if ($("svg").length > 0 ){
					console.log("清除圖表")
					$("svg").remove();
				}else{
					console.log("不清")
				}
				
				var temp =result["temp"];
				var Humi =result["humi"];
				var co2 = result["cotwo"];
				var TempOpt;
				var HumiOpt;
				var Co2Opt;
				
				if(temp>28){
					TempOpt={
						foregroundBorderWidth: 8,
						backgroundBorderWidth: 10,
						foregroundColor:'#E74C3C',//紅
						backgroundColor:'#D6DBDF',
						fillColor:'#eee',	//填充圓的顏色
						replacePercentageByText:result["temp"]+"°C",
						textColor:'#337ab7',//文字顏色
						textBelow:'true',
						text:"溫度",
						textStyle:'font-size: 14px;font-weight: 600;',
						percent: result["temp"],
						halfCircle: false,
					};
					
				}else if(temp>24){

					TempOpt={
						foregroundBorderWidth: 8,//前景圈的顏色
						backgroundBorderWidth: 10,//背景圓的顏色
						foregroundColor:'#F39C12',//橘
						backgroundColor:'#D6DBDF',
						fillColor:'#eee',	//填充圓的顏色
						replacePercentageByText:result["temp"]+"°C",
						textColor:'#337ab7',//文字顏色
						textBelow:'true',
						text:"溫度",
						textStyle:'font-size: 14px;font-weight: 600;',
						percent: result["temp"],
						halfCircle: false,
					};

				}else if(temp>18){

					TempOpt={
						foregroundBorderWidth: 8,//前景圈的顏色
						backgroundBorderWidth: 10,//背景圓的顏色
						foregroundColor:'#5cb85c',//綠
						backgroundColor:'#D6DBDF',
						fillColor:'#eee',	//填充圓的顏色
						replacePercentageByText:result["temp"]+"°C",
						textColor:'#337ab7',//文字顏色
						textBelow:'true',
						text:"溫度",
						textStyle:'font-size: 14px;font-weight: 600;',
						percent: result["temp"],
						halfCircle: false,
					};	

				}else{

					TempOpt={
						foregroundBorderWidth: 8,//前景圈的顏色
						backgroundBorderWidth: 10,//背景圓的顏色
						foregroundColor:'#3498DB',//藍
						backgroundColor:'#D6DBDF',
						fillColor:'#eee',	//填充圓的顏色
						replacePercentageByText:result["temp"]+"°C",
						textColor:'#337ab7',//文字顏色
						textBelow:'true',
						text:"溫度",
						textStyle:'font-size: 14px;font-weight: 600;',
						percent: result["temp"],
						halfCircle: false,
					};
				};

				if(Humi>70){
					HumiOpt={
						foregroundBorderWidth: 8,
						backgroundBorderWidth: 10,
						foregroundColor:'#E74C3C',//紅
						backgroundColor:'#D6DBDF',
						fillColor:'#eee',	//填充圓的顏色
						textColor:'#337ab7',//文字顏色
						textBelow:'true',
						text:"濕度",
						textStyle:'font-size: 14px;font-weight: 600;',
						percent: result["humi"],
						halfCircle: false,
					};
					
				}else if(Humi>60){
					HumiOpt={
						foregroundBorderWidth: 8,//前景圈的顏色
						backgroundBorderWidth: 10,//背景圓的顏色
						foregroundColor:'#F39C12',//橘
						backgroundColor:'#D6DBDF',
						fillColor:'#eee',	//填充圓的顏色
						textColor:'#337ab7',//文字顏色
						textBelow:'true',
						text:"濕度",
						textStyle:'font-size: 14px;font-weight: 600;',
						percent: result["humi"],
						halfCircle: false,
					};
				}else if(Humi>40){
					HumiOpt={
						foregroundBorderWidth: 8,//前景圈的顏色
						backgroundBorderWidth: 10,//背景圓的顏色
						foregroundColor:'#5cb85c',//綠
						backgroundColor:'#D6DBDF',
						fillColor:'#eee',	//填充圓的顏色
						textColor:'#337ab7',//文字顏色
						textBelow:'true',
						text:"濕度",
						textStyle:'font-size: 14px;font-weight: 600;',
						percent: result["humi"],
						halfCircle: false,
					};
				}else{
					HumiOpt={
						foregroundBorderWidth: 8,//前景圈的顏色
						backgroundBorderWidth: 10,//背景圓的顏色
						foregroundColor:'#3498DB',//藍
						backgroundColor:'#D6DBDF',
						fillColor:'#eee',	//填充圓的顏色
						textColor:'#337ab7',//文字顏色
						textBelow:'true',
						text:"濕度",
						textStyle:'font-size: 14px;font-weight: 600;',
						percent: result["humi"],
						halfCircle: false,
					};							
				};

				if(co2>600){
					Co2Opt={
						foregroundBorderWidth: 8,//前景圈的顏色
						backgroundBorderWidth: 10,//背景圓的顏色
						foregroundColor:'#E74C3C',//紅
						backgroundColor:'#D6DBDF',
						fillColor:'#eee',	//填充圓的顏色
						noPercentageSign:'true',//隱藏百分比
						textColor:'#337ab7',//文字顏色
						textBelow:'true',
						text:"二氧化碳",
						textStyle:'font-size: 14px;font-weight: 600;',
						percent: result["cotwo"],
						halfCircle: false,
						animationStep:80,
					};
					
				}else if(co2>400){
					Co2Opt={
						foregroundBorderWidth: 8,//前景圈的顏色
						backgroundBorderWidth: 10,//背景圓的顏色
						foregroundColor:'#F39C12',//橘
						backgroundColor:'#D6DBDF',
						fillColor:'#eee',	//填充圓的顏色
						noPercentageSign:'true',//隱藏百分比
						textColor:'#337ab7',//文字顏色
						textBelow:'true',
						text:"二氧化碳",
						textStyle:'font-size: 14px;font-weight: 600;',
						percent: result["cotwo"],
						halfCircle: false,
						animationStep:80,
					};
				}else{
					Co2Opt={
						foregroundBorderWidth: 8,//前景圈的顏色
						backgroundBorderWidth: 10,//背景圓的顏色
						foregroundColor:'#5cb85c',//綠
						backgroundColor:'#D6DBDF',
						fillColor:'#eee',	//填充圓的顏色
						noPercentageSign:'true',//隱藏百分比
						textColor:'#337ab7',//文字顏色
						textBelow:'true',
						text:"二氧化碳",
						textStyle:'font-size: 14px;font-weight: 600;',
						percent: result["cotwo"],
						halfCircle: false,
						animationStep:80,
					};							
				};

				$('#currTemp').circliful(TempOpt);
				$('#currHumi').circliful(HumiOpt);
				$('#currCo2').circliful(Co2Opt);  


			},
			error: function(result) {
				console.log(result);
			}
		});
		var options=$("#sel_room option:selected");
		
		if(options.length > 0){
			$("#roomname").html(options.text() +"-"+"當前數據");
		}
		
	};

	



</script>


   

</body>
</html>