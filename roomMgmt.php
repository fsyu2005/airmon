<?php 
ob_start();
session_start();
require_once './utility/ArrayList.php';
if(!isset($_SESSION['acct'])){
	// 尚未登入
	header("Location:index.php");
}
?>

<!DOCTYPE html>
<html>
<head>      
	<?php include("include/header.php") ?>
</head>
<style>
	.panel-heading{
		font-size:24px;
		font-weight:600;
	}
</style>

<body>
	<!-- 導覽列：Start -->
	<?php 
	if(isset($_SESSION['acct']) ){
		// echo "test";
		include("include/loginNavBar.php"); 
	}else{
		// echo "noooo";
		include("include/logoutNavBar.php"); 
	}
	?>
	<!-- END: 導覽列 -->


	<!-- 內容區：Start 程式碼寫在這 --> 
	<div class="container container_min">
	<h1>教室管理&nbsp;<small></small> </h1>
		<div class="row">
			<div class="col-md-12 ">
				<!-- 教室清單 -->
				<div class="panel panel-info">
					<div class="panel-heading">教室清單
						<button type="button" class="btn btn-danger ml-3 pull-right" data-toggle="modal" data-target="#removeroom">移除</button>
						<button type="button" class="btn btn-warning ml-3 pull-right" data-toggle="modal" data-target="#modroom">修改</button>
						<button type="button" class="btn btn-success ml-3 pull-right " data-toggle="modal" data-target="#addroom">新增</button>
					</div>
					<div class="panel-body">
					<table id="roomlist" class="table table-striped table-bordered" style="width:100%">
						<thead>
							<tr>
								<th>教室代碼</th>
								<th>教室名稱</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
						
					</table>
					</div>
				</div>
			</div>
		</div>
			 
		<!-- 新增教室 -->
		<div class="modal fade" id="addroom" role="dialog">
			<div class="modal-dialog modal-md">
				<div class="modal-content">
					<div class="modal-header green">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">新增教室</h4>
					</div>

					<div class="modal-body">
						<form id="addroomform" name="addroomform" method="post" onclick="return false">
							<div class="row">
								<!-- 管理者名稱 -->
								<div class="col-md-6">
									<div class="form-group">
										<label for="addRoomName">教室名稱:</label>
										<input type="text" class="form-control" name="addRoomName" id="addRoomName">
									</div>
								</div>
								<!-- 管理者密碼 -->
								<div class="col-md-6">
									<div class="form-group">
										<label for="addRoomCode">教室代碼:</label>
										<input type="text" class="form-control" name="addRoomCode" id="addRoomCode">
									</div>
								</div>
							</div>
						</form>
					</div>

					<div class="modal-footer">
						<button id="submit_add_room" class="btn btn-success pull-left"><span class="glyphicon glyphicon-plus"></span>新增</button>
						<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
					</div>

				</div>
			</div>
		</div>
		
		<!-- 教室修改 -->
		<div class="modal fade" id="modroom" role="dialog">
			<div class="modal-dialog modal-md">
				<div class="modal-content">
					<div class="modal-header yellow">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">教室修改</h4>
					</div>

					<div class="modal-body">
						<form id="modroomform" name="modroomform" method="post" onclick="return false">
							<div class="row">
								<!-- 選擇教室代碼 -->
								<div class="col-md-6">
									<div class="form-group">
										<label for="modRoomCode">選擇教室代碼:</label>
										<select class="form-control" name="modRoomCode" id="modRoomCode">
											
										</select>
									</div>
								</div>
								<!-- 輸入教室名稱 -->
								<div class="col-md-6">
									<div class="form-group">
										<label for="newRoomNmae">教室名稱:</label>
										<input type="text" class="form-control" name="newRoomNmae" id="newRoomNmae">
									</div>
								</div>
								
							</div>
						</form>
					</div>

					<div class="modal-footer">
						<button id="submit_mod_room" class="btn btn-warning pull-left"><span class="glyphicon glyphicon-pencil"></span>修改</button>
						<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
					</div>

				</div>
			</div>
		</div>
		
		<!-- 移除教室 -->
		<div class="modal fade" id="removeroom" role="dialog">
			<div class="modal-dialog modal-md">
				<div class="modal-content">
					<div class="modal-header red">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">移除教室</h4>
					</div>

					<div class="modal-body">
					<form id="removeroomform" name="removeroomform" method="post" onclick="return false">
						<div class="row">
							<!-- 選擇教室代碼 -->
							<div class="col-md-6">
								<div class="form-group">
									<label for="removeRoomCode">選擇教室代碼:</label>
									<select class="form-control" name="removeRoomCode" id="removeRoomCode">
								
									</select>
								</div>
							</div>
							
						</div>
					</form>						
					</div>

					<div class="modal-footer">
						<button id="submit_remove_room" class="btn btn-danger pull-left"><span class="glyphicon glyphicon-remove"></span>移除</button>
						<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
					</div>

				</div>
			</div>
		</div>


	</div>
	<!-- 內容區：END -->

	

    <!-- Footer列：Start -->
	<?php include("include/footer.php") ?>
	<!-- Footer列：End -->
</body>
</html>

<script>
	
	$(document).ready(function(){
		
		var t = $('#roomlist').DataTable({
            "language": {
				"processing":   "處理中...",
				"loadingRecords": "載入中...",
				"lengthMenu":   "顯示 _MENU_ 項結果",
				"zeroRecords":  "沒有符合的結果",
				"info":         "顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
				"infoEmpty":    "顯示第 0 至 0 項結果，共 0 項",
				"infoFiltered": "(從 _MAX_ 項結果中過濾)",
				"infoPostFix":  "",
				"search":       "搜尋:",
				"paginate": {
					"first":    "第一頁",
					"previous": "上一頁",
					"next":     "下一頁",
					"last":     "最後一頁"
				},
				"aria": {
					"sortAscending":  ": 升冪排列",
					"sortDescending": ": 降冪排列"
				}
			}
        });
		// 載入教室清單
		$.ajax({
			url: "Controller.php?command=GetRoom",
			type: "POST",
			dataType: "json",
				success: function(list) {
					for (i = 0; i < list.length; i++) {
						$("#modRoomCode").append("<option value='"+list[i]["code"]+"'>"+list[i]["code"]+"</option>");
						$("#removeRoomCode").append("<option value='"+list[i]["code"]+"'>"+list[i]["code"]+"</option>");
						
						t.row.add( [
							list[i]["code"],
							list[i]["name"]
						] ).draw( false );

					}
					
				},
				error: function() {
					alert("ERROR!!!");
				}
		});
		
		// 新增教室
		$("#submit_add_room").on('click', function(){
			if($("#addRoomName").val().replace(/\s+/g,"")==""){
				alert("未填寫教室名稱");
				eval("document.addroomform['addRoomName'].focus()");       
			}else if($("#addRoomCode").val().replace(/\s+/g,"")==""){
				alert("未填寫教室代碼");
				eval("document.addroomform['addRoomCode'].focus()");    
			}else{
				$.ajax({
				url: 'Controller.php?command=AddRoom',
				type : "POST",
				dataType : 'json',
				data : $("#addroomform").serialize(),
					success : function(result) {
						alert(result);
					},
					error: function(result) {
						console.log(result);
					}
				});
				
				location.replace('roomMgmt.php');
	
			}
			
		});
	
		// 修改教室名稱
		$("#submit_mod_room").on('click', function(){
			if($("#newRoomNmae").val().replace(/\s+/g,"")==""){
				alert("請輸入教室名稱");
				eval("document.modroomform['newRoomNmae'].focus()");       
			}else{
				$.ajax({
				url: 'Controller.php?command=ModifyRoom',
				type : "POST",
				dataType : 'json',
				data : $("#modroomform").serialize(),
					success : function(result) {
						alert(result);
					},
					error: function(result) {
						console.log(result);
					}
				});
	
				location.replace('RoomMgmt.php');
			}
			
		});
	
		// 移除密碼
		$("#submit_remove_room").on('click', function(){

			
			if (confirm("確定要刪除教室?") == true){
				$.ajax({
				url: 'Controller.php?command=RemoveRoom',
				type : "POST",
				dataType : 'json',
				data : $("#removeroomform").serialize(),
					success : function(result) {
						alert(result);
					},
					error: function(result) {
						console.log(result);
					}
				});
				
				location.replace('RoomMgmt.php');
			}else{
				location.replace('RoomMgmt.php');
			}
				
		});
		
	
	});
		
	
		
	</script>