<?php 
ob_start();
session_start();
require_once './utility/ArrayList.php';
// if(!isset($_SESSION['acct'])){
// 	// 尚未登入
// 	header("Location:index.php");
// }
?>
<!DOCTYPE html>
<html>
<head>      


<?php include("include/header.php") ?>

<style>
.panel-heading{
		font-size:24px;
		font-weight:600;
	}
.lable2{
	font-size:26px;
	font-weight:100;
}
.room_title{
    font-size: 76px;
    font-weight: 600;
    text-align: center;
    height: 100%;
    line-height: 400%;
    vertical-align: middle;
}
.input-group-addon{
	background: #286090;
	border-color: #204d74;
	color: #FFF;
}

</style>
</head>


<body onload="$('#submit_form').click();">
	<!-- 導覽列：Start -->
	<?php 
	if(isset($_SESSION['acct']) ){
		// echo "test";
		include("include/loginNavBar.php"); 
	}else{
		// echo "noooo";
		include("include/logoutNavBar.php"); 
	}
	?>
	<!-- END: 導覽列 -->

	<!-- 內容區：Start 程式碼寫在這 --> 
	<div class='lmask' id="mask">
		<div class="loader"></div>
	</div>
	<div class="container container_min">	
		
		<!-- 程式碼寫在這 -->
        <h1>依時間查詢&nbsp;<small>教室品質異常次數</small> </h1>
		<div class="alert alert-info">
			<div class="row">

				<div class="col-md-12">
					<h3 style="margin: 0 0 15px 0;"><strong>查詢條件</strong></h3>
					<strong><h4>說明：</h4></strong>
					<p>　　※選擇欲查詢之日期區間後，即可查詢教室的異常事件之次數</p>
					<p>　　※若未選擇日期區間，預設會帶入7日內的資料進行異常事件查詢</p>
				</div>
				
				<form id="roomform" name="roomform" method="post">
					
					<div class="col-md-4">
						<div class="form-group">
							<label for="startdate">日期區間:</label>
							<div class="input-daterange input-group" id="datepicker">
								<input type="text" class="form-control" name="start" id="start" value="2018/12/1"/>
								<span class="input-group-addon">-</span>
								<input type="text" class="form-control" name="end" id="end" value="<?=date("Y/m/d")?>"/>
								<a id="submit_form" class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-search"></span> 查詢</a>
							</div>
						</div>
					</div>
					
				</form>
				
			</div>
		</div>	
		<div class="row">
			<div class="col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">教室品質異常次數表</div>
				<div class="panel-body">
					<table id="DetailTable" class="table table-striped table-bordered" style="width:100%">
						<thead>
							<tr>
								<th>教室</th>
								<th>事件</th>
								<th>發生次數/小時</th>
								<th>日期</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
						
					</table>
				</div>
			</div>
			</div>
		</div>
    </div>		
	<!-- 內容區：END -->
    <!-- Footer列：Start -->
	<?php include("include/footer.php") ?>
	<!-- Footer列：End -->
<script>
	var t;
	$(document).ready(function() {
		
		// 日期區間
		$('.input-daterange').datepicker({
			format: "yyyy/mm/dd",
    		orientation: "bottom right",
			todayBtn: "linked",
			clearBtn: true,
			language: "zh-TW",
			
		});

		t = $('#DetailTable').DataTable({
            "language": {
				"processing":   "處理中...",
				"loadingRecords": "載入中...",
				"lengthMenu":   "顯示 _MENU_ 項結果",
				"zeroRecords":  "沒有符合的結果",
				"info":         "顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
				"infoEmpty":    "顯示第 0 至 0 項結果，共 0 項",
				"infoFiltered": "(從 _MAX_ 項結果中過濾)",
				"infoPostFix":  "",
				"search":       "搜尋:",
				"paginate": {
					"first":    "第一頁",
					"previous": "上一頁",
					"next":     "下一頁",
					"last":     "最後一頁"
				},
				"aria": {
					"sortAscending":  ": 升冪排列",
					"sortDescending": ": 降冪排列"
				}
			}
		});
	
		// 查詢
		$("#submit_form").click( function(){
			
		
			$("#mask").css("display", "block");


			if($("#start").val().replace(/\s+/g,"")==""){
				alert("請選擇時間區間");
				$("#start").focus();
			}else if($("#end").val().replace(/\s+/g,"")==""){
				alert("請選擇時間區間");
				$("#end").focus();
			}else {
				$.ajax({
				url: 'Controller.php?command=GetRoomEvent',
				type : "POST",
				dataType : 'json',
				data : $("#roomform").serialize(),
					success : function(result) {
						
						t.clear().draw();
						for (i = 0; i < result["Temp"].length; i++) {
							t.row.add( [
								result["Temp"][i]["name"],
								"<font color='blue'>溫度過高</font>",
								result["Temp"][i]["count_temp"],
								result["Temp"][i]["date"],
							] ).draw( false );
						}	
						for (i = 0; i < result["Humi"].length; i++) {
							t.row.add( [
								result["Humi"][i]["name"],
								"<font color='green'>濕度過低</font>",
								result["Humi"][i]["count_humi"],
								result["Humi"][i]["date"],
							] ).draw( false );
						}
						for (i = 0; i < result["Co2"].length; i++) {
							t.row.add( [
								result["Co2"][i]["name"],
								"<font color='red'>二氧化炭過高</font>",
								result["Co2"][i]["count_co2"],
								result["Co2"][i]["date"],
							] ).draw( false );
						}
						$("#mask").css("display", "none");
					},
					error: function(result) {
						console.log(result);
						t.clear().draw();
						alert("無紀錄");
					}
				});
			}
			
		});

		
		// $("#submit_form").click();	
		

	});

	
	
	
</script>
</body>
</html>


