<?php 
ob_start();
session_start();
require_once './utility/ArrayList.php';
if(isset($_SESSION['acct'])){
	// 以登入直接轉跳welcome畫面
	echo "<script>location.href='welcome.php'</script>";
}
?>
<!DOCTYPE html>
<html>
<head>      
<?php include("include/header.php") ?>
</head>
<style>
	.panel-heading{
		font-size:24px;
		font-weight:400;
		}
	
</style>
<body>

	<!-- 導覽列：Start -->
	<?php 
	if(isset($_SESSION['acct']) ){
		// echo "test";
		include("include/loginNavBar.php"); 
	}else{
		// echo "noooo";
		include("include/logoutNavBar.php"); 
	}
	?>
	<!-- END: 導覽列 -->


	<!-- 內容區：Start -->
	<div class="container container_min">
		<div style="font-size:20px;margin:5px;font-weight:bold">
			<i class="glyphicon glyphicon-th-list" style="color:#803232"></i> 歡迎使用 空氣品質監控系統
		</div>
		<hr>  	
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="panel panel-primary">
						<div class="panel-heading">
						系統登入
						</div>
						<div class="panel-body">
							<form role="form" class="form-horizontal" action="/airmon/Controller.php?command=Login" method="post">
									<div class="col-md-12 mb-3">
										<div class="input-group">
											<span class="input-group-addon" ><i class="glyphicon glyphicon-user"></i></span>
											<input id="text" type="text" class="form-control input-lg" name="username" placeholder="請輸入帳號..." value="admin">
										</div>
									</div>
									<div class="col-md-12 mb-3">
										<div class="input-group">
											<span class="input-group-addon" ><i class="glyphicon glyphicon-pencil"></i></span>
											<input id="password" type="password" class="form-control input-lg" name="password" placeholder="請輸入密碼..." value="admin">
										</div>
									</div>
									<div class="col-md-3"></div>
									<div class="col-md-3 mb-3">  
										<input type="submit" class="btn btn-block btn-primary" value="登入"> 
									</div>  
									<div class="col-md-3"> 
										<input type="reset" class="btn btn-block btn-default" value="清空"> 
									</div> 
									<div class="col-md-3"></div> 		
							</form> 
						</div>
					</div>
				</div>
			</div>

			
			
		</div>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
			
		
	</div>
	<!-- 內容區：End -->


	<!-- Footer列：Start -->
	<?php include("include/footer.php") ?>
	<!-- Footer列：End -->

</body>
</html>