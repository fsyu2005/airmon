<?php

/**
* 
* Singleton design pattern for Service Locator. On server, we need to be sure that 
* there is only one object for this type.
* 
*
* @author FengShuo Yu
**/

class ServiceLocator
{
	private static $instances = array(); // only me can access it.

	public static function &getInstance($className)
	{
		$namespace = ""; // this is used for naming, forget it for now

		$baseName = str_replace($namespace,"",$className);

		$className = $namespace.$baseName;

		// For each service, only one copy exists on service side
		if (!isset(ServiceLocator::$instances[$className]))
		{
			require_once "service/" . $className .".php";
			ServiceLocator::$instances[$className] = new $className;
		}

		return ServiceLocator::$instances[$className];
	}
}

?>