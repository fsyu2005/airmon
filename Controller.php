<?php
ob_start();
session_start();

class Controller
{
	public static function getAction($actionName)
	{
		$findme = "?";
		$pos = strpos($actionName, $findme); //函數返回字符串在另一個字符串中第一次出現的位置 strpos(string,find,start)
		if ($pos === false)
		{ // 如果沒有找到	//===比較兩者是否相等(還包含類型的相等) 
			$actionName = $actionName;
		}
		else
		{
			$actionName = substr($actionName, 0, $pos); //函數返回字符串的一部分
		}

		$className = $actionName . "Action";
		require_once "action/" . $className . ".php";
		return new $className;
	}
}

/*command*/
//parse
$actionObj = Controller::getAction($_GET['command']);

// 同步bean
$actionObj->syncModelWithGUI();
//execute
$list= $actionObj->execute();
?>