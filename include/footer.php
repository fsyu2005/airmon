<style>
	footer{
		background: #2c5f67;
		color: #FFF;
		padding: 10px 10px 5px 10px;
		text-align: center;
		position:fixed;
		bottom:0;
		width:100%;
	}

</style>

<footer>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<p>崑山科技大學 / 資訊管理系 / 空氣品質監控系統 - 版權所有 </p>
				<p>Copyright © 2018 MIS Inc. All rights reserved.</p>
			</div>
		</div>
	</div>
</footer>


