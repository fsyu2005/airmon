<title>崑山科技大學 空氣品質監控系統</title>
<meta charset="utf-8"> 
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />


<!-- Bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- circliful -->
<link href="css/jquery.circliful.css" rel="stylesheet" type="text/css" />
<script src="js/jquery.circliful.min.js"></script>

<!-- Chart.js -->
<script src="js/Chart.bundle.js"></script>

<!-- Bootstrap-Slider -->
<link href="vendor/Bootstrap-Slider/css/bootstrap-slider.css" rel="stylesheet" type="text/css" />
<script src="vendor/Bootstrap-Slider/bootstrap-slider.js"></script>

<!-- datatables -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

<!-- datepicker -->

<script type="text/javascript" src="datepicker/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="datepicker/bootstrap-datepicker.css"/>
<script type="text/javascript" src="datepicker/bootstrap-datepicker.zh-TW.min.js"></script>

<!-- Google Icons -->
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

<!-- other -->
<link href="font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" />
<link href="css/my.css" rel="stylesheet" type="text/css" />
<script src="js/my.js"></script>