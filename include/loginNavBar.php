<?php 
ob_start();
require_once './utility/ArrayList.php';
include_once './bean/Account.php';
?>

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="index.php">崑山科技大學 空氣品質監控系統</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">數據查詢<span class="caret"></span></a>
          <ul class="dropdown-menu">
            
            <li><a href="board_current.php">瀏覽儀表板</a></li>
            <li><a href="board_history.php">瀏覽歷史</a></li>
            <li><a href="board_event.php">查詢異常次數</a></li>
            
          </ul>
        </li>
        
        <li><a href="roomMgmt.php">教室管理</a></li>
        
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">帳戶管理(<?=$_SESSION['user']?>)<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <!-- <li><a href="adminMgmt.php">管理者維護</a></li> -->
            <li><a data-toggle="modal" data-target="#modadm">管理者密碼修改</a></li>
            
            <li><a href="Controller.php?command=Logout">登出</a></li>
          </ul>
        </li>
        
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">相關連結<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="http://www.ksu.edu.tw" target="_blank">學校首頁</a></li>
            <li><a href="http://cca.ksu.edu.tw/cca/public/auth/index" target="_blank">校務研究系統</a></li>
            <li><a href="http://www.abri.gov.tw/tw/periodical/show/75/1280/p/print#" target="_blank">建築研究簡訊第75期《專題報導》</a></li>
          </ul>
        </li>
        
        <li><a href="contact_us.php">關於我們</a></li>
      </ul>
    </div>
  </div>
</nav>


<!-- 密碼修改 -->
<div class="modal fade" id="modadm" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header yellow">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">密碼修改</h4>
      </div>

      <div class="modal-body">
        <form id="modadmform" name="modadmform" method="post" onclick="return false">
          <div class="row">
            <!-- 管理者 -->
            <div class="col-md-6">
              <div class="form-group">
                <label for="modadmin">管理者:</label>
                <input type="text" class="form-control" name="modadmin" id="modadmin" value="admin" disabled="disabled">
              </div>
            </div>

          </div>	
          <div class="row">	
            <!-- 輸入舊密碼 -->
            <div class="col-md-6">
              <div class="form-group">
                <label for="oldpwd">舊密碼:</label>
                <input type="password" class="form-control" name="oldpwd" id="oldpwd">
              </div>
            </div>
            <!-- 管理者密碼 -->
            <div class="col-md-6">
              <div class="form-group">
                <label for="newpwd">新密碼:</label>
                <input type="password" class="form-control" name="newpwd" id="newpwd">
              </div>
            </div>
          </div>
        </form>
      </div>

      <div class="modal-footer">
        <button id="submit_mod_adm" class="btn btn-warning pull-left"><span class="glyphicon glyphicon-pencil"></span>修改</button>
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

<script>
	
  $(document).ready(function(){
  
  
  
    // 修改密碼
    $("#submit_mod_adm").on('click', function(){
      if($("#oldpwd").val().replace(/\s+/g,"")==""){
              alert("請輸入舊密碼");
              eval("document.modadmform['oldpwd'].focus()");       
          }else if($("#newpwd").val().replace(/\s+/g,"")==""){
              alert("請輸入新密碼");
              eval("document.modadmform['newpwd'].focus()");    
          }else if($("#modadmin").val().replace(/\s+/g,"")==""){
              alert("請輸入管理者名稱");
              eval("document.modadmform['modadmin'].focus()");    
          }else{
        $.ajax({
        url: 'Controller.php?command=ModifyAdmin',
        type : "POST",
        dataType : 'json',
        data : $("#modadmform").serialize(),
          success : function(result) {
            alert(result);
          },
          error: function(result) {
            console.log(result);
          }
        });
  
        // location.replace('adminMgmt.php');
        location.replace('Controller.php?command=Logout');
          }
      
    });
  
  });
    
  
    
  </script>