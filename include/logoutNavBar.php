<?php 
ob_start();
require_once './utility/ArrayList.php';
include_once './bean/Account.php';
?>

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="index.php">崑山科技大學 空氣品質監控系統</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">

        <li><a href="index.php">登入</a></li>

        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">數據查詢<span class="caret"></span></a>
          <ul class="dropdown-menu">
            
            <li><a href="board_current.php">瀏覽儀表板</a></li>
            <li><a href="board_history.php">瀏覽歷史</a></li>
            <li><a href="board_event.php">查詢異常次數</a></li>
            
          </ul>
        </li>

        
       
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">相關連結<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="http://www.ksu.edu.tw" target="_blank">學校首頁</a></li>
            <li><a href="http://cca.ksu.edu.tw/cca/public/auth/index" target="_blank">校務研究系統</a></li>
            <li><a href="http://www.abri.gov.tw/tw/periodical/show/75/1280/p/print#" target="_blank">建築研究簡訊第75期《專題報導》</a></li>
          </ul>
        </li>
        
        <li><a href="contact_us.php">關於我們</a></li>
      </ul>
    </div>
  </div>
</nav>


