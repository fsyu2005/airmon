<?php
include_once ('.'.'/ServiceLocator.php');
include_once ('.'.'/utility/ArrayList.php');
include_once ('.'.'/bean/Account.php');

class LogoutAction
{
	public function doView($page)
	{		
		header("Location:$page");
		//include $page;
		exit();
		// Ref: http://www.webmasterworld.com/forum88/782.htm
	}
	
	// 同步 網頁 及 Bean的資料
	public function syncModelWithGUI(){
		
	}
	
	// 執行。
	public function execute()
	{
		// 先取出 相關的Service 物件
		$_SESSION = array();
		/***删除sessin id.由于session默认是基于cookie的，所以使用setcookie删除包含session id的cookie.***/
		unset($_SESSION['acct']);
		$page = "index.php";
		$this->doView($page);
	}
}

?>