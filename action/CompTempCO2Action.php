<?php
include_once ('.'.'/ServiceLocator.php');
include_once ('.'.'/utility/ArrayList.php');
include_once ('.'.'/bean/Account.php');
include_once ('.'.'/service/BoardsService.php');

class CompTempCO2Action
{
	public function doView($page)
	{		
		header("Location:$page");
		//include $page;
		exit();
		// Ref: http://www.webmasterworld.com/forum88/782.htm
	}
	
	// 同步 網頁 及 Bean的資料
	public function syncModelWithGUI(){
		
		
	}
	
	// 執行。
	public function execute() {
		$room_code = $_POST["room_code"];		
		$rows = $_POST["rows"];
		
		$service  = new BoardsService();
		
		$data_list = $service->getTempCO2ByID($room_code, $rows);
		$_SESSION['data_list'] = $data_list;
		$_SESSION['room_code'] = $room_code;

		//die(var_dump($data_list));
		$page = "comp_temp_co2.php";
		$this->doView($page);
	}
}

?>