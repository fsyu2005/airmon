<?php
include_once ('.'.'/ServiceLocator.php');
include_once ('.'.'/utility/ArrayList.php');
include_once ('.'.'/bean/Account.php');
include_once ('.'.'/service/BoardsService.php');

class GetRoomDetailAction
{
	public function doView($page)
	{		
		header("Location:$page");
		//include $page;
		exit();
		// Ref: http://www.webmasterworld.com/forum88/782.htm
	}
	
	// 同步 網頁 及 Bean的資料
	public function syncModelWithGUI(){
		
	}
	
	// 執行。
	public function execute() 	{
		
		$service  = new BoardsService();	

		if(empty($_POST["sel_room"])){
			$_POST["sel_room"] = "pc1";
		}
		$room_code = $_POST["sel_room"];		
		$start_time = $_POST["start"];	
		$end_time = $_POST["end"];

		

		$room_data = $service->getRoomDetail($room_code,$start_time,$end_time);	

		$_SESSION["room_data"] = $room_data;
		// return json
		echo json_encode($room_data);

		// $page = "temp_detail.php";
		// $this->doView($page);
	}
}

?>