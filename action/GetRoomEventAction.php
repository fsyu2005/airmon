<?php
include_once ('.'.'/ServiceLocator.php');
include_once ('.'.'/utility/ArrayList.php');
include_once ('.'.'/bean/Account.php');
include_once ('.'.'/service/BoardsService.php');

class GetRoomEventAction
{
	public function doView($page)
	{		
		header("Location:$page");
		//include $page;
		exit();
		// Ref: http://www.webmasterworld.com/forum88/782.htm
	}
	
	// 同步 網頁 及 Bean的資料
	public function syncModelWithGUI(){
		
	}
	
	// 執行。
	public function execute() 	{
		
		$service  = new BoardsService();	
		
				
		$start_time = $_POST["start"];	
		$end_time = $_POST["end"];

		$room_data["Temp"] = $service->getRoomEventTemp($start_time,$end_time);	
		$room_data["Humi"] = $service->getRoomEventHumi($start_time,$end_time);
		$room_data["Co2"] = $service->getRoomEventCo2($start_time,$end_time);
		
		// return json
		echo json_encode($room_data);
		
		
		
		
		// $page = "temp_detail.php";
		// $this->doView($page);
	}
}

?>