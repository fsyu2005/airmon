<?php
include_once ('.'.'/ServiceLocator.php');
include_once ('.'.'/utility/ArrayList.php');
include_once ('.'.'/bean/Account.php');
include_once ('.'.'/service/AdminService.php');

class GetAdminAction
{
	public function doView($page)
	{		
		header("Location:$page");
		//include $page;
		exit();
		// Ref: http://www.webmasterworld.com/forum88/782.htm
	}
	
	// 同步 網頁 及 Bean的資料
	public function syncModelWithGUI(){
		
		
	}
	
	// 執行。
	public function execute()
	{
		$service  = new AdminService();
		$user = $service->getAdminUser();
		
		// return json
		echo json_encode($user);

		// $page = "adminMgmt.php";
		// $this->doView($page);
	}
}

?>