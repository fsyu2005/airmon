<?php
include_once ('.'.'/ServiceLocator.php');
include_once ('.'.'/utility/ArrayList.php');
include_once ('.'.'/bean/Account.php');
include_once ('.'.'/service/RoomService.php');

class AddRoomAction
{
	public function doView($page)
	{		
		header("Location:$page");
		//include $page;
		exit();
		// Ref: http://www.webmasterworld.com/forum88/782.htm
	}
	
	// 同步 網頁 及 Bean的資料
	public function syncModelWithGUI(){
		
		
	}
	
	// 執行。
	public function execute()
	{
		$service  = new RoomService();

		$addRoomName = $_POST["addRoomName"];
		$addRoomCode = $_POST["addRoomCode"];
		// 寫入資料
		$statement = $service->addRome($addRoomName,$addRoomCode);
		
		if($statement)
        {
        $result = "新增成功\n教室名稱:$addRoomName\n教室代碼:$addRoomCode";
        echo json_encode($result);
        return;
        }
        else
        {
        $result = "新增失敗!\n教室代碼重複";
        echo json_encode($result);
        return;
        }
		
		
	}
}

?>