<?php
include_once ('.'.'/ServiceLocator.php');
include_once ('.'.'/utility/ArrayList.php');
include_once ('.'.'/bean/Account.php');
include_once ('.'.'/service/BoardsService.php');

class DrawCurveByRoomAction {
	public function doView($page)
	{		
		header("Location:$page");
		//include $page;
		exit();
		// Ref: http://www.webmasterworld.com/forum88/782.htm
	}
	
	// 同步 網頁 及 Bean的資料
	public function syncModelWithGUI(){
		
		
	}
	
	// 執行。
	public function execute() 	{
		$room_code = $_GET["room_code"];		
		$rows = $_GET["rows"];
		$service  = new BoardsService();		
		$data_list = $service->getCurveDataByID($room_code, $rows);		
		$_SESSION['data_list'] = $data_list;
		$_SESSION['room_code'] = $room_code;

		$page = "curve_map.php";
		$this->doView($page);
	}
}

?>