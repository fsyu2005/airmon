<?php
include_once('.'.'/datasource/DataSource.php');
include_once('.'.'/utility/ArrayList.php');
include_once('.'.'/bean/Account.php');
?>

<?php 
class RoomService {

	public function getRoom( ) {	
		
		$connDB = new DataSource();
	
		//$query = "SELECT * FROM mobile_data ORDER BY curr_time DESC LIMIT 100";
		$query = "SELECT * FROM `room` ORDER BY `name`";

		// 執行SQL語法
		$statement = $connDB->executeQuery($query);
		
		// SQL回傳值存到 $result 裡
        $result = $connDB->fetchAll($statement);
	
		foreach($result as $key => $value) {
			$rows[] = $value;
			
		}
		
		return $rows;

	}
	//新增教室
	public function addRome($name,$code){

		$connDB = new DataSource();
		$query = "INSERT INTO room (`code`, `name`) SELECT '$code', '$name' FROM DUAL WHERE NOT EXISTS(SELECT `code` FROM room WHERE `code` = '$code');";
		
		// 執行SQL語法
		$statement = $connDB->executeQuery($query);
		// 回傳受影響行數結果
		$result = $connDB->affectedrows();
		
		
		return $result;	
	}
	// 修改教室
	public function modRome($name,$code){

		$connDB = new DataSource();
		$query = "UPDATE room set `name` = '$name' WHERE `code` = '$code'";
		
		// 執行SQL語法
		$statement = $connDB->executeQuery($query);
		// 回傳受影響行數結果
		$result = $connDB->affectedrows();
		
		
		return $result;	
	}
	// 移除教室
	public function removeRome($code){

		$connDB = new DataSource();
		$query = "DELETE FROM room WHERE `code` = '$code';";
		
		// 執行SQL語法
		$statement = $connDB->executeQuery($query);
		// 回傳受影響行數結果
		$result = $connDB->affectedrows();
		
		
		return $result;	
	}
}

?>