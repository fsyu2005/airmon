<?php
include_once('.'.'/datasource/DataSource.php');
include_once('.'.'/utility/ArrayList.php');
include_once('.'.'/bean/Account.php');
?>

<?php 
class BoardsService {
	

	public function getBoardDataByID($room_code) {		
		$items = 50;

		if($items==null || $items==""){
			$items =50;
		}

		$connDB = new DataSource();
	
		//$query = "SELECT * FROM mobile_data ORDER BY curr_time DESC LIMIT 100";
		$query = "SELECT * from ". 
      			" (SELECT * FROM mobile_data  where room_code='".$room_code."' order by curr_time DESC limit ".$items.") as temptb".
      			" order by curr_time";

		// 執行SQL語法
		$statement = $connDB->executeQuery($query);
		
		// SQL回傳值存到 $result 裡
        $result = $connDB->fetchAll($statement);
	
		foreach($result as $key => $value) {
			$rows[] = $value;
		}
		//die(json_encode($result));
		
		//die(var_dump($rows));
		return $rows;

	}

	public function getTempCO2ByID($room_code, $rows) {	
		if($rows == null){
			$rows = 30; // using one month
		}

		$connDB = new DataSource();
		$query = "select * from (SELECT * FROM mobile_data where room_code='".$room_code."' ORDER BY ".
		         " curr_time DESC LIMIT $rows) as mygod order by curr_time DESC";
		$statement = $connDB->executeQuery($query);
        $result = $connDB->fetchAll($statement);
	
		foreach($result as $key => $value) {
			$list[] = $value;
		}
		
		return $list;

	}

	public function getCurveDataByID($room_code, $rows) {	
		if($rows == null){
			$rows = 30; // using one month
		}

		$connDB = new DataSource();
		$query = "select * from (SELECT * FROM mobile_data  where room_code='".$room_code."'  ORDER BY curr_time DESC LIMIT $rows) as mygod order by curr_time";
		$statement = $connDB->executeQuery($query);
        $result = $connDB->fetchAll($statement);
	
		foreach($result as $key => $value) {
			$list[] = $value;
		}
		
		return $list;

	}

	//取得最後一筆紀錄(及時數據)
	public function getLatestData($room_code) {		

		$connDB = new DataSource();
		$query = "SELECT id,format(temp, 1) as temp,humi,cotwo,curr_time,room_code FROM mobile_data where room_code = '$room_code' ORDER BY curr_time DESC LIMIT 1 ";
		$statement = $connDB->executeQuery($query);
        $result = $connDB->fetchAll($statement);
	
		foreach($result as $key => $value) {
			$rows[] = $value;
		}

		return $rows[0];

	}
	
	// 依教室取得數據(教室,日期)
	public function getRoomDetail($room_code,$start,$end) {	
		

		$connDB = new DataSource();
		
		$query = "SELECT room_code,ROUND(avg(temp),2) as 'temp' ,ROUND(avg(humi),2) as 'humi' ,ROUND(avg(cotwo),0) as 'co2',date_format(curr_time,'%m-%d %H:00') as 'time'
		FROM mobile_data
		WHERE room_code ='$room_code' and  DATE(curr_time) <= '$end' and DATE(curr_time) >= '$start'
		
		GROUP BY time
		";
		
		$statement = $connDB->executeQuery($query);
        $result = $connDB->fetchAll($statement);
	
		foreach($result as $key => $value) {
			$list[] = $value;
		}
		
		return $list;

	}
	//取得view_count_temp異常紀錄(日期)
	public function getRoomEventTemp($start,$end) {	
		// if($rows == null){
		// 	$rows = 30; 
		// }

		$connDB = new DataSource();
		
		$query = "SELECT * 
					FROM `view_count_temp` 
					LEFT JOIN room
					ON room.`code` = view_count_temp.room_code
					WHERE DATE(date) <= '$end' and DATE(date) >= '$start';";
		
		$statement = $connDB->executeQuery($query);
		$result = $connDB->fetchAll($statement);
		$row = $connDB->affectedrows($statement);
		
		

		if($row == 0){
			return $row;
		}else{
			foreach($result as $key => $value) {
				$data[] = $value;
			}
			return $data;
		}
		
		
		

	}
	
	//取得view_count_humi異常紀錄(日期)
	public function getRoomEventHumi($start,$end) {	
		// if($rows == null){
		// 	$rows = 30; 
		// }

		$connDB = new DataSource();
		
		$query = "SELECT * 
					FROM `view_count_humi` 
					LEFT JOIN room
					ON room.`code` = view_count_humi.room_code
					WHERE DATE(date) <= '$end' and DATE(date) >= '$start';";
		
		$statement = $connDB->executeQuery($query);
		$result = $connDB->fetchAll($statement);
		$row = $connDB->affectedrows($statement);
			
		if($row == 0){
			return $row;
		}else{
			foreach($result as $key => $value) {
				$data[] = $value;
			}
			return $data;
		}

	}

	//取得view_count_co2異常紀錄(日期)
	public function getRoomEventCo2($start,$end) {	
		// if($rows == null){
		// 	$rows = 30; 
		// }

		$connDB = new DataSource();
		
		$query = "SELECT * 
					FROM `view_count_co2` 
					LEFT JOIN room
					ON room.`code` = view_count_co2.room_code
					WHERE DATE(date) <= '$end' and DATE(date) >= '$start';";
		
		$statement = $connDB->executeQuery($query);
        $result = $connDB->fetchAll($statement);
		$row = $connDB->affectedrows($statement);


		if($row == 0){
			return $row;
		}else{
			foreach($result as $key => $value) {
				$data[] = $value;
			}
			return $data;
		}

	}

	
}

?>