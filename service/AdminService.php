<?php
include_once('.'.'/datasource/DataSource.php');
include_once('.'.'/utility/ArrayList.php');
include_once('.'.'/bean/Account.php');
?>

<?php 
class AdminService {

	// 取得管理者清單
	public function getAdminUser() {		
		
		$connDB = new DataSource();
	
		
		$query = "SELECT * FROM userprof";

		// 執行SQL語法
		$statement = $connDB->executeQuery($query);
		
		// SQL回傳值存到 $result 裡
        $result = $connDB->fetchAll($statement);
	
		foreach($result as $key => $value) {
			$rows[] = $value["username"];
		}
		return $rows;
	}

	// 新增管理者
	public function addAccount($acc,$pwd){

		$connDB = new DataSource();
		
		$query = "INSERT INTO userprof( username ,`password` )
		VALUES ( '$acc','$pwd' )";
		// 執行SQL語法
		$statement = $connDB->executeQuery($query);	
		// 回傳受影響行數結果
		$result = $connDB->affectedrows();
		
		
		return $result;	
	}
	// 修改管理者
	public function modAccount($acc,$old,$new){

		$connDB = new DataSource();
		$query = "UPDATE userprof
					SET `password` = '$new'
					WHERE 
						username = '$acc'and `password` = '$old';";
		
		// 執行SQL語法
		$statement = $connDB->executeQuery($query);
		// 回傳受影響行數結果
		$result = $connDB->affectedrows();
		
		
		return $result;	
	}

	public function removeAccount($acc,$pwd){

		$connDB = new DataSource();
		$query = "DELETE FROM userprof WHERE userprof.username = '$acc' and userprof.`password` = '$pwd';";
		
		// 執行SQL語法
		$statement = $connDB->executeQuery($query);
		// 回傳受影響行數結果
		$result = $connDB->affectedrows();
		
		
		return $result;	
	}
}

?>