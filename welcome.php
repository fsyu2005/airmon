<?php 
ob_start();
session_start();
require_once './utility/ArrayList.php';
if(!isset($_SESSION['acct'])){
	// 尚未登入
	header("Location:index.php");
}
?>
<!DOCTYPE html>
<html>

<head>
	<?php include("include/header.php") ?>


</head>


<body>
	<!-- 導覽列：Start -->
	<?php 
	if(isset($_SESSION['acct']) ){
		// echo "test";
		include("include/loginNavBar.php"); 
	}else{
		// echo "noooo";
		include("include/logoutNavBar.php"); 
	}
	?>
	<!-- END: 導覽列 -->

	<!-- 內容區：Start -->

	<div class="container container_min">
		<br>
		<div class="row">
			<div class="col-md-12">
				<div class="carousel slide" id="carousel-555157">
					<ol class="carousel-indicators">
						<li class="active" data-slide-to="0" data-target="#carousel-555157">
						</li>
						<li data-slide-to="1" data-target="#carousel-555157">
						</li>
						<li data-slide-to="2" data-target="#carousel-555157">
						</li>
					</ol>
					<div class="carousel-inner">
						<div class="item active">
							<img src="http://bir.ksu.edu.tw/bir/public/img/banner_1.min.png">
							
						</div>
						<div class="item">
							<img src="http://bir.ksu.edu.tw/bir/public/img/banner_2.min.png">
							
						</div>
						<div class="item">
							<img src="http://bir.ksu.edu.tw/bir/public/img/banner_3.min.png">
							
						</div>
					</div> <a class="left carousel-control" href="#carousel-555157" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
					<a class="right carousel-control" href="#carousel-555157" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
				</div>
			</div>
			<br>
			<div class="col-md-12">
				<h1 class="text-center">~※計畫背景※~</h1>
					<p style="text-align: center;">由於在台灣南部夏天的高溫以及冬天的空氣品質不佳</p>
					<p style="text-align: center;">因此我們透過蒐集各項控制舒適度條件的數據來分析並且加以改善</p>
					<p style="text-align: center;">建構可即時感測、監督空氣品質且提醒使用者的監控系統平台</p>
					<p style="text-align: center;">進一步配合控制裝置的操控</p>
					<p style="text-align: center;">來達到環境品質自動化管理的目標。</p>
			
				</div>


		</div>
	</div>
	<!-- 內容區：END -->



	<!-- Footer列：Start -->
	<?php include("include/footer.php") ?>
	<!-- Footer列：End -->

</body>

</html>