<?php

class Account
{
	//宣告成員
	public $id;
	public $uid; // 這個是登入的帳號 T093000000
	public $name;
	public $pw;	
	public $username; // 這個是顯示用的，放中文名字
	public $role;
	public $isLogin = false;
	public $email;
	
	//建構子
	function __construct($username = 'unname' , $pw = 'unpassword')
	{
		$this->username = $username;
		$this->pw = $pw;
	}
	
	function set_isLogin($login)
	{
		$this->isLogin = $login;
	}
	
	function get_isLogin()
	{
		return $this->isLogin;
	}
	
	
	function set_email($email)
	{
		$this->email = $email;
	}
	
	function get_email()
	{
		return $this->email;
	}
	
	
	function set_role($role)
	{
		$this->role = $role;
	}
	
	function get_role()
	{
		return $this->role;
	}
	
	function set_id($id)
	{
		$this->id = $id;
	}
	
	function get_id()
	{
		return $this->id;
	}
	
	function set_uid($uid)
	{
		$this->uid = $uid;
	}
	
	function get_uid()
	{
		return $this->uid;
	}
	
	function set_username($username)
	{
		$this->username = $username;
	}
	function get_username()
	{
		return $this->username;
	}
	function set_name($name)
	{
		$this->name = $name;
	}
	function get_name()
	{
		return $this->name;
	}
	function set_pw($pw)
	{
		$this->pw = $pw;
	}
	function get_pw()
	{
		return $this->pw;
	}
}

?>