<?php 
ob_start();
session_start();
require_once './utility/ArrayList.php';
// if(!isset($_SESSION['acct'])){
// 	// 尚未登入
// 	header("Location:index.php");
// }
?>
<!DOCTYPE html>
<html>
<head>      


<?php include("include/header.php") ?>

<style>
.panel-heading{
		font-size:24px;
		font-weight:600;
	}
.lable2{
	font-size:26px;
	font-weight:100;
}
.room_title{
    font-size: 76px;
    font-weight: 600;
    text-align: center;
    height: 100%;
    line-height: 400%;
    vertical-align: middle;
}
.input-group-addon{
	background: #286090;
	border-color: #204d74;
	color: #FFF;
}
</style>
</head>


<body onload="$('#submit_form').click();">
	<!-- 導覽列：Start -->
	<?php 
	if(isset($_SESSION['acct']) ){
		// echo "test";
		include("include/loginNavBar.php"); 
	}else{
		// echo "noooo";
		include("include/logoutNavBar.php"); 
	}
	?>
	<!-- END: 導覽列 -->

	<!-- 內容區：Start 程式碼寫在這 --> 
	<div class="container container_min">		
		
		<!-- 程式碼寫在這 --> 
        <h1>溫/濕度及二氧化碳&nbsp;<small>歷史資料</small> </h1>
		<div class="alert alert-info">
			<div class="row">
				<div class="col-md-12">
					<h3 style="margin: 0 0 15px 0;"><strong>查詢條件</strong></h3>
					<strong><h4>說明：</h4></strong>
					<p>　　※選擇欲查詢之教室後，即可得知此教室即時的溫度、濕度、二氧化碳濃度數據</p>
					<p>　　※若未選擇日期區間，預設會帶入7日內的歷史資料</p>
				</div>
				<!-- 選擇教室 -->
				
				<form id="roomform" name="roomform" method="post">
					<div class="col-md-3">
						<div class="form-group">
							<label for="sel_room">選擇教室:</label>
							<select class="form-control" id="sel_room" name="sel_room">
								
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="startdate">日期區間:</label>
							<div class="input-daterange input-group" id="datepicker">
								<input type="text" class="form-control" name="start" id="start" value="2018/12/1"/>
								<span class="input-group-addon">-</span>
								<input type="text" class="form-control" name="end" id="end" value="<?=date("Y/m/d")?>"/>
								<a id="submit_form" class="input-group-addon btn btn-primary""><span class="glyphicon glyphicon-search"></span> 查詢</a>
							</div>
						</div>
					</div>
					
				</form>
				

				
			</div>
    	</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-success">
					<div class="panel-heading" id="roomname"></div>
					<div class="panel-body">
					<canvas id="DetailChart"></canvas>
					</div>
				</div>
			</div>
		</div>
    </div>
    
	
	<!-- 內容區：END -->

	

    <!-- Footer列：Start -->
	<?php include("include/footer.php") ?>
	<!-- Footer列：End -->
<script>
	
	$(document).ready(function() {
		// 載入教室清單
		$.ajax({
			url: "Controller.php?command=GetRoom",
			type: "POST",
			dataType: "json",
				success: function(list) {
					var classroom ;
					for (i = 0; i < list.length; i++) {
						if( i==0 ){
							classroom = list[i]["name"];
						}
						$("#sel_room").append("<option value='"+list[i]["code"]+"'>"+list[i]["name"]+"</option>");
						$("#roomname").html(classroom +"-"+ "歷史資料");
					}	
					
				},
				error: function() {
					alert("ERROR!!!");
				}
		});

		// 日期區間
		$('.input-daterange').datepicker({
			format: "yyyy/mm/dd",
    		orientation: "bottom right",
			todayBtn: "linked",
			clearBtn: true,
			language: "zh-TW",
			
		});
		
		var list;
		// 查詢
		
		$("#submit_form").click( function(){
			
			if($("#start").val().replace(/\s+/g,"")==""){
				alert("請選擇時間區間");
				$("#start").focus();
			}else if($("#end").val().replace(/\s+/g,"")==""){
				alert("請選擇時間區間");
				$("#end").focus();
			}else {
				$.ajax({
				url: 'Controller.php?command=GetRoomDetail',
				type : "POST",
				dataType : 'json',
				data : $("#roomform").serialize(),
					success : function(result) {
						
						console.log("上次"+list);
						for (let i = 0; i < list; i++) {
							example.data.labels.splice(-1, 1);//移除項目

							example.data.datasets.forEach(function(dataset) {
								dataset.data.pop();
							});

							example.update();

						}
						
						

						for (i = 0; i < result.length; i++) {
							example.data.labels.push(result[i]["time"]);
							example.data.datasets[0].data.push(result[i]["temp"]);
							example.data.datasets[1].data.push(result[i]["humi"]);
							example.data.datasets[2].data.push(result[i]["co2"]);
							
							example.update();
							
						}
						list = result.length ;
						console.log("載入後"+list);
					},
					error: function(result) {
						console.log(result);
						alert("無此紀錄");
					}
				});
				var options=$("#sel_room option:selected");
			
				$("#roomname").html(options.text()+"-"+"歷史資料");
			}
			
		});

		var ctx = document.getElementById( "DetailChart" ),
			example = new Chart(ctx, {
			// 參數設定[註1]
			type: "line", // 圖表類型
			data: {
				datasets: [{
					label: "溫度", 
					yAxisID: '溫度',
					borderColor: "#E74C3C",
					fill: false,
					data:[]
					// radius: 0,
				},{
					label: "濕度", // 標籤
					yAxisID: '溫度',
					borderColor: "#3e95cd",
					fill: false,
					data:[]
					// radius: 0,
				},{ 
					label: "co2",
					yAxisID: 'CO2',
					borderColor: "#8e5ea2",
					fill: false,
					data:[]
					// radius: 0,
				}]
			},
			options: {
				scales: {
					yAxes: [{
							id: '溫度',
							type: 'linear',
							position: 'left',
						},{
							id: 'CO2',
							type: 'linear',
							position: 'right',
							// ticks: {
							// max: 2000,
							// min: 100
							// }
						}
					]
				},
				
			}
    	});
		

	});

	function removeData(chart) {
		chart.data.labels.pop();
		chart.data.datasets.forEach((dataset) => {
			dataset.data.pop();
		});
		chart.update();
	};
	
	
</script>
</body>
</html>
