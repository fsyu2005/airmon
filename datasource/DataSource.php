<?php

/**
 * Data Source object.
 * 
 * @author FengShuo Yu
 * @copyright 2011
 */

class DataSource{
    private $conn;
	
	// 建立資料庫連線
	public function __construct() {
		
		$servername = "127.0.0.1";
		// $servername = "120.114.101.83";
		$username = "root";
		$password = "password";
		// $password = "";
		$dbname = "air";
		
		$this->conn = mysqli_connect($servername, $username, $password, $dbname);
		$this->conn->set_charset("utf8");
		
		if (!$this->conn) {
			die ("MySQL Connect Error");
		}

	}
	
	// 執行SQL語法
    public function executeQuery($query){
		return mysqli_query($this->conn, $query);
		
    }

	// 回傳取得結果
	public function fetchAll($result) {
		return mysqli_fetch_all($result, MYSQLI_ASSOC);
	}	

	// 回傳受影響行數結果
	public function affectedrows() {
		return mysqli_affected_rows($this->conn);
	}	
	
}

?>